export LESS="-Xr"
export EDITOR='emacs -q'
export PATH=$HOME/bin:$PATH
export PATH=/usr/local/go/bin:$HOME/go/bin:$PATH
[[ -d "$HOME/.krew" ]] && export PATH="$HOME/.krew/bin:$PATH"
export SDKMAN_DIR="$HOME/.sdkman"
#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
export PATH=$PATH:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Library/TeX/texbin:/usr/local/share/dotnet:~/.dotnet/tools
export PATH="/Users/chris/.pyenv/shims:$PATH"
