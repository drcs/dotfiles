# Antigen package manager for zsh
curl -L git.io/antigen > ~/.antigen.zsh

# SDKMAN!
curl -s https://get.sdkman.io | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"

sdk install java 8.0.302-open

# SBT starting point for Scala projects
# (which will then each use particular versions of SBT and Scala)
sdk install sbt

# Scala (for playing around with Scala shell)
sdk install scala

pyenv install 3.10.0
pyenv global 3.10.0

# Stuff needed for Emacs LSP Python support
pip install --upgrade python-lsp-server
pip install --upgrade pylsp-mypy
pip install --upgrade pyls-isort

echo "Now install the Hack font: https://github.com/source-foundry/Hack"
echo "And in Emacs, do 'M-x all-the-icons-install-fonts'"
