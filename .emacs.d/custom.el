(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(yasnippet-snippets yaml-mode which-key use-package treemacs-projectile treemacs-magit treemacs-icons-dired string-inflection sbt-mode rustic multiple-cursors monokai-theme lsp-ui lsp-python-ms lsp-pyright lsp-metals lsp-jedi lsp-ivy julia-mode jenkinsfile-mode helm-projectile helm-lsp graphviz-dot-mode go-mode git-gutter-fringe+ flycheck expand-region doom-modeline dockerfile-mode counsel company-box)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(mode-line ((t (:height 0.9))))
 '(mode-line-inactive ((t (:height 0.9)))))
