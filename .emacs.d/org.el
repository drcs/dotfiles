;; My org-mode customizations

;; These are the suggested defaults from http://orgmode.org/org.html#Introduction
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)
(global-set-key (kbd "C-c b") 'org-iswitchb)

(setq-default org-support-shift-select 1)
(setq org-descriptive-links nil)
(setq org-startup-indented t)
(setq org-list-indent-offset 0)

;; From https://github.com/syl20bnr/spacemacs/issues/13255#issuecomment-592998372
;; src block indentation / editing / syntax highlighting
(setq org-src-fontify-natively t
      org-src-window-setup 'current-window ;; edit in current window
      org-src-strip-leading-and-trailing-blank-lines t
      org-src-preserve-indentation t ;; do not put two spaces on the left
      org-src-tab-acts-natively t)

(if (string= system-name "fokine")
    (setq org-dir "~/Box/org")
  (setq org-dir "~/vcs/github.com/dr-cs/org"))

(setq org-agenda-files
      (directory-files-recursively org-dir "\\.org$"))

(setq org-enforce-todo-dependencies t
      org-tags-column 100)
      ;; org-priority-highest 1
      ;; org-priority-lowest 64
      ;; org-priority-default 10)

(use-package ob-go)

;; ;; http://www.howardism.org/Technical/Emacs/journaling-org.html
;; (defun get-journal-file-today ()
;;   "Return filename for today's journal entry."
;;   (let ((daily-name (format-time-string "%Y-%m-%d.org")))
;;     (expand-file-name (concat org-journal-dir daily-name))))

;; (defun journal-file-today ()
;;   "Create and load a journal file based on today's date."
;;   (interactive)
;;   (find-file (get-journal-file-today)))

;; (global-set-key (kbd "C-c f j") 'journal-file-today)

(use-package org-journal
  :ensure t
  :init
  ;; ;; Change default prefix key; needs to be set before loading org-journal
  ;; (setq org-journal-prefix-key "C-c j")
  :config
  (setq org-journal-file-type 'daily
        org-extend-today-until 0 ;; the default
        org-journal-find-file #'find-file
        org-journal-dir (concat org-dir "/journal/")
        org-journal-date-format "%Y-%m-%d %a"
        org-journal-file-format "%Y-%m-%d.org"))

;; Graphviz dot language
(org-babel-do-load-languages
     'org-babel-load-languages
     '((dot . t)
       (python . t)
       (ditaa . t)))

(require 'ox-latex)
(unless (boundp 'org-latex-classes)
  (setq org-latex-classes nil))

(add-to-list 'org-latex-classes
             '("exam"
               "\\documentclass{exam}"
               ;; org-latex-export requires the header, but the exam
               ;; class doesn't want one. Comment out,
               ;; org-latex-export and exam class both happy
               ("\\begin{questions} %% %s"
                "\\end{questions}"
                "\\begin{questions} %% %s"
                "\\end{questions}")
               ("\\question %s " . "\\question* %s")
               ("\\begin{parts} %s"
                "\\end{parts}"
                "\\begin{parts} %s"
                "\\end{parts}")))

(setq org-latex-listings t)
(setq org-confirm-babel-evaluate nil)
(setq org-hierarchical-todo-statistics t)

;; See https://github.com/bastibe/org-journal#carry-over
(defun my-old-carryover (old_carryover)
  (save-excursion
    (let ((matcher (cdr (org-make-tags-matcher org-journal-carryover-items))))
      (dolist (entry (reverse old_carryover))
        (save-restriction
          (narrow-to-region (car entry) (cadr entry))
          (goto-char (point-min))
          (org-scan-tags '(lambda ()
                            (org-set-tags ":carried:"))
                         matcher org--matcher-tags-todo-only))))))

(setq org-journal-handle-old-carryover 'my-old-carryover)

;; (let* ((variable-tuple (cond ((x-list-fonts "Source Sans Pro") '(:font "Source Sans Pro"))
;;                                  ((x-list-fonts "Lucida Grande")   '(:font "Lucida Grande"))
;;                                  ((x-list-fonts "Verdana")         '(:font "Verdana"))
;;                                  ((x-family-fonts "Sans Serif")    '(:family "Sans Serif"))
;;                                  (nil (warn "Cannot find a Sans Serif Font.  Install Source Sans Pro."))))
;;            (base-font-color     (face-foreground 'default nil 'default))
;;            (headline           `(:inherit default :weight bold :foreground ,base-font-color)))
;; (custom-theme-set-faces 'user
;;                         `(org-level-8 ((t (,@headline ,@variable-tuple))))
;;                         `(org-level-7 ((t (,@headline ,@variable-tuple))))
;;                         `(org-level-6 ((t (,@headline ,@variable-tuple))))
;;                         `(org-level-5 ((t (,@headline ,@variable-tuple))))
;;                         `(org-level-4 ((t (,@headline ,@variable-tuple :height 1.1))))
;;                         `(org-level-3 ((t (,@headline ,@variable-tuple :height 1.25))))
;;                         `(org-level-2 ((t (,@headline ,@variable-tuple :height 1.5))))
;;                         `(org-level-1 ((t (,@headline ,@variable-tuple :height 1.75))))
;;                         `(org-document-title ((t (,@headline ,@variable-tuple :height 1.5 :underline nil)))))

;; (setq orb-blank-before-new-entry
;;       '((heading . t)
;;         (plain-list-item . nil)))

;; (setq org-cycle-separator-lines 1)
