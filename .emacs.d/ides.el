;; Packages and settings to provide IDE-like features for particular langauges

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Specific langauge modes


(use-package go-mode
  :interpreter
  ("go" . go-mode)
  :config
  (gofmt-before-save))


;; Enable scala-mode for highlighting, indentation and motion commands
(use-package scala-mode
  :interpreter
    ("scala" . scala-mode))

;; Enable sbt mode for executing sbt commands
(use-package sbt-mode
  :commands sbt-start sbt-command
  :config
  ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
  ;; allows using SPACE when in the minibuffer
  (substitute-key-definition
   'minibuffer-complete-word
   'self-insert-command
   minibuffer-local-completion-map)
   ;; sbt-supershell kills sbt-mode:  https://github.com/hvesalai/emacs-sbt-mode/issues/152
   (setq sbt:program-options '("-Dsbt.supershell=false"))
)

(use-package yaml-mode
  :pin melpa
  :mode "\\.y[a]ml$")

(use-package json-mode
  :pin melpa
  :mode "\\.json$")

(use-package rustic)

(use-package julia-mode
  :pin melpa)

(use-package groovy-mode
  :mode
  "Jenkinsfile"
  :config
  (setq groovy-basic-offset 2))

;;(use-package jenkinsfile-mode)

(use-package dockerfile-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; General programming support

(use-package flycheck
  :pin melpa
  :init (global-flycheck-mode))


(use-package yasnippet
  :config
  (yas-global-mode))
  ;; :demand
  ;; :config
  ;; (setq yas-snippet-dirs
  ;;       '(;; personal snippets
  ;;         "~/.emacs.d/snippets"
  ;;         ;; You'll need to clone https://github.com/AndreaCrotti/yasnippet-snippets
  ;;         ;; and customize this based on where you clone it.
  ;;         "~/vcs/github.com/AndreaCrotti/yasnippet-snippets"
  ;;         )))

(use-package yasnippet-snippets)

;; Use company-capf as a completion provider.
;;
;; To Company-lsp users:
;;   Company-lsp is no longer maintained and has been removed from MELPA.
;;   Please migrate to company-capf.
(use-package company
  :hook
  (scala-mode . company-mode)
  (go-mode . company-mode)
  :config
  (setq lsp-completion-provider :capf)
  (add-hook 'lsp-managed-mode-hook (lambda () (setq-local company-backends '((company-capf :with company-yasnippet))))))

(use-package company-box
  :hook (company-mode . company-box-mode))

(use-package expand-region
  :demand
  :config
  :bind
   ("C-=" . er/expand-region))

(use-package graphviz-dot-mode
  :demand
  :config
  (setq-default graphviz-dot-auto-indent-on-semi nil)
  (setq-default graphviz-dot-indent-width 4)
  (setq graphviz-dot-auto-indent-on-semi nil))

(use-package magit
  :demand
  :bind
  ("C-x g" . magit-status))

(use-package git-gutter-fringe+
  :demand
  :config
  (if window-system (global-git-gutter+-mode t)))

(use-package ivy)

(use-package projectile
  :demand
  :pin melpa-stable
  :init
  (setq projectile-use-git-grep t)
  (projectile-mode +1)
  :bind
  (:map projectile-mode-map
        ("M-s-p" . projectile-command-map))
  :config
  (projectile-global-mode t)
  (setq projectile-project-search-path '(("~/vcs/github.com" . 2)
                                         ("~/vcs/gitlab.com" . 2))))

(use-package helm)

(use-package helm-projectile
  :bind   (("M-s-f" . helm-projectile-find-file)
           ("M-s-F" . helm-projectile-grep)))


(use-package monokai-theme
  :demand
  :config
  (load-theme 'monokai t))


(require 'expand-region)
(global-set-key (kbd "C-=") 'er/expand-region)

;; Use the Debug Adapter Protocol for running tests and debugging
;; Posframe is a pop-up tool that must be manually installed for dap-mode
(use-package posframe)

(use-package dap-mode
  :pin melpa-stable
  :hook
  (lsp-mode . dap-mode)
  (lsp-mode . dap-ui-mode))

;; (use-package counsel ;; brings ivy and swiper as dependencies
;;   :pin melpa-stable)

;; (use-package lsp-ivy
;;   :pin melpa
;;   :commands lsp-ivy-workspace-symbol)

(use-package treemacs
  :ensure t
  :defer t
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (setq treemacs-collapse-dirs                   (if treemacs-python-executable 3 0)
          treemacs-deferred-git-apply-delay        0.5
          treemacs-directory-name-transformer      #'identity
          treemacs-display-in-side-window          t
          treemacs-display-current-project-exclusively t
          treemacs-eldoc-display                   'simple
          treemacs-file-event-delay                5000
          treemacs-file-extension-regex            treemacs-last-period-regex-value
          treemacs-file-follow-delay               0.2
          treemacs-file-name-transformer           #'identity
          treemacs-follow-after-init               t
          treemacs-expand-after-init               t
          treemacs-find-workspace-method           'find-for-file-or-pick-first
          treemacs-git-command-pipe                ""
          treemacs-goto-tag-strategy               'refetch-index
          treemacs-indentation                     2
          treemacs-indentation-string              " "
          treemacs-is-never-other-window           nil
          treemacs-max-git-entries                 5000
          treemacs-missing-project-action          'ask
          treemacs-move-forward-on-expand          nil
          treemacs-no-png-images                   nil
          treemacs-no-delete-other-windows         t
          treemacs-project-follow-cleanup          nil
          treemacs-persist-file                    (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-position                        'left
          treemacs-read-string-input               'from-child-frame
          treemacs-recenter-distance               0.1
          treemacs-recenter-after-file-follow      nil
          treemacs-recenter-after-tag-follow       nil
          treemacs-recenter-after-project-jump     'always
          treemacs-recenter-after-project-expand   'on-distance
          treemacs-litter-directories              '("/node_modules" "/.venv" "/.cask")
          treemacs-show-cursor                     nil
          treemacs-show-hidden-files               t
          treemacs-silent-filewatch                nil
          treemacs-silent-refresh                  nil
          treemacs-sorting                         'alphabetic-asc
          treemacs-select-when-already-in-treemacs 'move-back
          treemacs-space-between-root-nodes        t
          treemacs-tag-follow-cleanup              t
          treemacs-tag-follow-delay                1.5
          treemacs-text-scale                      nil
          treemacs-user-mode-line-format           nil
          treemacs-user-header-line-format         nil
          treemacs-wide-toggle-width               70
          treemacs-width                           35
          treemacs-width-increment                 1
          treemacs-width-is-initially-locked       t
          treemacs-workspace-switch-cleanup        nil)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;;(treemacs-resize-icons 44)

    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode 'always)

    (pcase (cons (not (null (executable-find "git")))
                 (not (null treemacs-python-executable)))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple)))

    (treemacs-hide-gitignored-files-mode nil))
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
        ("C-x t d"   . treemacs-select-directory)
        ("C-x t p"   . treemacs-display-current-project-exclusively)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))

(use-package treemacs-projectile
  :after treemacs projectile
  :ensure t)

(use-package treemacs-icons-dired
  :after treemacs dired
  :ensure t
  :pin melpa-stable
  :config (treemacs-icons-dired-mode))

(use-package treemacs-magit
  :after treemacs magit
  :ensure t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Language Server Protocol

;; lsp-mode is the Emacs front end, which needs a back end to work.
;; For each language for which you enable lsp-mode, there must be a
;; back end language server on the system PATH.  You can find language
;; servers at https://emacs-lsp.github.io/lsp-mode/page/languages/.
;; Some languages, like Python, don't have a single vendor-supported
;; language server but a set of partially broken or abandoned servers
;; (notably, Microsoft made the de-facto standard Python language
;; server, python-language-server, but then abandoned it and moved to
;; a proprietary version that can only be used with their VS Code
;; plugin -- Embrace, Extend, Extinguish).  So here I list the
;; vendor-supported (yay Google!), community-consensus standard, or
;; best-I-could-find language servers that I use.

;; Go: https://github.com/golang/tools/tree/master/gopls

;; Python: https://github.com/python-lsp/python-lsp-server

;; Scala: https://scalameta.org/metals/docs/editors/emacs -- I use
;; coursier to install the server

(use-package lsp-mode
  :defer t
  :pin
  melpa

  :hook
  (go-mode . lsp)
  (python-mode . lsp)
  (scala-mode . lsp)
  (lsp-mode . lsp-lens-mode)
  (lsp-mode . lsp-enable-which-key-integration)

  :config
  ;; Uncomment following section if you would like to tune lsp-mode performance according to
  ;; https://emacs-lsp.github.io/lsp-mode/page/performance/
  ;;       (setq gc-cons-threshold 100000000) ;; 100mb
  ;;       (setq read-process-output-max (* 1024 1024)) ;; 1mb
  ;;       (setq lsp-idle-delay 0.500)
  ;;       (setq lsp-log-io nil)
  (setq lsp-completion-provider :capf)
  (setq lsp-prefer-flymake nil)
  (setq lsp-enable-snippet t)
  :bind-keymap
  ;; I rebind all s- keybindings to M-s- so they don't conflict with
  ;; Ubuntu s- keybindings
  ("M-s-l" . lsp-command-map)

  :commands
  lsp)

(use-package lsp-pyright
  :ensure t
  :config
  (lsp-register-custom-settings
   '(("pyls.plugins.pyls_mypy.enabled" t t)
     ("pyls.plugins.pyls_mypy.live_mode" nil t)
     ;; ("pyls.plugins.pyls_black.enabled" t t)
     ("pyls.plugins.pyls_isort.enabled" t t)))
  :hook (python-mode . (lambda ()
                          (require 'lsp-pyright)
                          (lsp))))  ; or lsp-deferred
;; (use-package lsp-jedi
;;   :ensure t
;;   :config
;;   (with-eval-after-load "lsp-mode"
;;     (add-to-list 'lsp-disabled-clients 'pyls)
;;     (add-to-list 'lsp-enabled-clients 'jedi)))


;; (use-package lsp-python-ms
;;   :ensure t
;;   :init
;;   ;; for executable of language server, if it's not symlinked on your PATH
;;   (setq lsp-python-ms-executable
;;         "~/vcs/github.com/microsoft/python-language-server/output/bin/Release/osx-x64/publish/Microsoft.Python.LanguageServer")
;;   :hook (python-mode . (lambda ()
;;                           (require 'lsp-python-ms)
;;                           (lsp))))  ; or lsp-deferred

(use-package lsp-treemacs :commands lsp-treemacs-errors-list)

(use-package helm-lsp :commands helm-lsp-workspace-symbol)

;; Add metals backend for lsp-mode
(use-package lsp-metals
  :pin
  melpa)

;; Enable nice rendering of documentation on hover
;;   Warning: on some systems this package can reduce your emacs responsiveness significally.
;;   (See: https://emacs-lsp.github.io/lsp-mode/page/performance/)
;;   In that case you have to not only disable this but also remove from the packages since
;;   lsp-mode can activate it automatically.
(use-package lsp-ui
  :commands lsp-ui-mode)


(defun lsp-go-install-save-hooks ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)
