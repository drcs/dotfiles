;; Customization for stuff built-in to Emacs

;; Basics
(set-face-attribute 'default nil :family "Hack" :height 150)
(setq-default visible-bell 1)
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq-default show-paren-mode 1)
(if window-system (tool-bar-mode 0))
(if (boundp 'aquamacs-version) (tabbar-mode 0))
(setq-default linum-format "%4d ")
(set-scroll-bar-mode 'right)
(add-hook 'text-mode-hook 'turn-on-visual-line-mode)
(setq-default visual-line-fringe-indicators '(nil nil))
(setq-default word-wrap t)
(setq-default column-number-mode t)
(show-paren-mode 1)
(delete-selection-mode 1)
(transient-mark-mode 1)
;;(setq-default ispell-program-name (chomp (shell-command-to-string "which ispell")))
(add-hook 'before-save-hook 'delete-trailing-whitespace)

(when (fboundp 'electric-indent-mode) (electric-indent-mode -1))

;; Got this from https://colinxy.github.io/software-installation/2016/09/24/emacs25-easypg-issue.html
(setq epa-pinentry-mode 'loopback)

;; From https://www.emacswiki.org/emacs/ExecPath
(defun set-exec-path-from-shell-PATH ()
  "Set up Emacs' `exec-path' and PATH environment variable to match that used by the user's shell. This is particularly useful under macOS, where GUI apps are not started from a shell."
  (interactive)
  (let ((path-from-shell
         (replace-regexp-in-string
		  "[ \t\n]*$" ""
          ;; Not a --login shell to ensure that pyenv shims are at
          ;; head of path.  Still can't figure out what's going on
          ;; with path config, but it's been messed up ever since I
          ;; installed Microsoft dotnet as part of the Microsoft
          ;; Python language server
          (shell-command-to-string "$SHELL -c 'echo $PATH'"))))
    (message "Path from shell %s" path-from-shell)
    (setenv "PATH" path-from-shell)
    (setq exec-path (split-string path-from-shell path-separator))))

(set-exec-path-from-shell-PATH)


;; UTF-8 everywhere
(prefer-coding-system 'utf-8)
(set-language-environment 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)

;; linum-mode everywhere, except where it doesn't belong
(if (version<= "26.0.50" emacs-version )
  (global-display-line-numbers-mode)
  (progn
    (require 'linum)
    (setq linum-disabled-modes-list '(eshell-mode
                                      wl-summary-mode
                                      compilation-mode
                                      dired-mode
                                      speedbar-mode
                                      mu4e-main-mode
                                      mu4e-about-mode
                                      mu4e-view-mode
                                      mu4e-headers-mode
                                      doc-view-mode))
    (defun linum-on ()
      (unless (or (minibufferp)
                  (member major-mode linum-disabled-modes-list)
                  (and (not (eq (buffer-name) "*scratch*"))
                       (string-match "*" (buffer-name))))
        (linum-mode 1)))
    (global-linum-mode 1)
    (setq linum-eager nil)))

;; Keybindings
(setq mac-option-modifier 'super)
(setq mac-command-modifier 'meta)
(global-set-key (kbd "M-g") 'goto-line)

;; From http://www.emacswiki.org/emacs/SmoothScrolling
;; Scroll one line at a time (less "jumpy" than defaults)
(setq-default mouse-wheel-scroll-amount '(1 ((shift) . 1)))
(setq-default mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq-default mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq-default scroll-step 1) ;; keyboard scroll one line at a time

;; Wider than 80 for general editing, e.g., org-mode tables. Use
;; fill-column-indicator for programming modes
(setq display-fill-column-indicator-column 80)
(display-fill-column-indicator-mode)

(add-to-list 'initial-frame-alist '(width . 180))
(add-to-list 'default-frame-alist '(width . 180))
(add-to-list 'initial-frame-alist '(height . 60))
(add-to-list 'default-frame-alist '(height . 60))

;; (defun my:window-setup-hook ()
;;   (when (and (string= system-type "gnu/linux") window-system)
;;     (toggle-frame-maximized)
;;     (let* ((dconf-entry
;;             (shell-command-to-string
;;              "dconf read /com/ubuntu/user-interface/scale-factor"))
;;            (scale-factor (progn (string-match "'[eD][FD]P-*1': \\([0-9]+\\)[,\}]"
;;                                               dconf-entry)
;;                                 (string-to-int (match-string 1 dconf-entry))))
;;            (text-width (truncate (/ 96 (/ scale-factor 8.0))))
;;            (text-height (truncate (/ 50 (/ scale-factor 8.0)))))
;;       (message "set-frame-size is %dx%d, scale-factor is %s"
;;                text-width text-height scale-factor)
;;       (set-frame-size (selected-frame) text-width text-height))))

;; (setq window-setup-hook 'my:window-setup-hook)

(when (string= system-type "darwin")
  (setq dired-use-ls-dired t
        insert-directory-program "gls"))

(setq dired-listing-switches "-aBhl")

(use-package all-the-icons
  :ensure t
  :custom
  (all-the-icons-install-fonts t))

(use-package markdown-mode
  :ensure t
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))


(use-package multiple-cursors
  :demand
  :bind
  ("C-c m" . mc/edit-lines)
  ("C->" . mc/mark-next-like-this)
  ("C-<" . mc/mark-previous-like-this)
  ("C-c C-<" . mc/mark-all-like-this)
  :config
  (setq mc/always-run-for-all t))

(use-package which-key
  :config (which-key-mode))

(use-package string-inflection
  :bind
  ("C-c C-u" . string-inflection-all-cycle))

;; (use-package doom-modeline
;;   :ensure t
;;   :init (doom-modeline-mode 1))

(use-package doom-modeline
  :hook (after-init . doom-modeline-mode)
  :custom-face
  (mode-line ((t (:height 0.9))))
  (mode-line-inactive ((t (:height 0.9))))
  :custom
  (doom-modeline-height 1)
  (doom-modeline-bar-width 1)
  (doom-modeline-icon t)
  (doom-modeline-unicode-fallback t)
  (doom-modeline-major-mode-icon t)
  (doom-modeline-major-mode-color-icon t)
  (doom-modeline-lsp t)
  (doom-modeline-match-count t)
  (doom-modeline-project-detection 'auto)
  (doom-modeline-buffer-file-name-style 'auto)
  (doom-modeline-buffer-state-icon t)
  (doom-modeline-buffer-modification-icon t)
  (doom-modeline-minor-modes nil)
  (doom-modeline-enable-word-count nil)
  (doom-modeline-buffer-encoding t)
  (doom-modeline-indent-info nil)
  (doom-modeline-checker-simple-format t)
  (doom-modeline-vcs-max-length 20)
  (doom-modeline-env-version t)
  (doom-modeline-irc-stylize 'identity)
  (doom-modeline-github-timer nil)
  (doom-modeline-gnus-timer nil))

;; (use-package doom-themes
;;   :ensure t
;;   :config
;;   ;; Global settings (defaults)
;;   (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
;;         doom-themes-enable-italic t) ; if nil, italics is universally disabled
;;   (load-theme 'doom-one t)

;;   ;; Enable flashing mode-line on errors
;;   (doom-themes-visual-bell-config)
;;   ;; ;; Enable custom neotree theme (all-the-icons must be installed!)
;;   ;; (doom-themes-neotree-config)
;;   ;; ;; or for treemacs users
;;   (setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
;;   (doom-themes-treemacs-config)
;;   ;; Corrects (and improves) org-mode's native fontification.
;;   (doom-themes-org-config))

;; (use-package doom-modeline
;;   :custom-face
;;   (mode-line ((t (:height 0.85))))
;;   (mode-line-inactive ((t (:height 0.85))))
;;   :custom
;;   (doom-modeline-height 15)
;;   (doom-modeline-bar-width 6)
;;   (doom-modeline-lsp t)
;;   (doom-modeline-github t)
;;   (doom-modeline-buffer-encoding nil)
;;   (doom-modeline-major-mode-icon t))
